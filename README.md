
A module for accessing a redis database by means of a restful api.

CAUTION: This project is **under** development.

## Instalation ##

Install dependencies and redis database.

Please refer to:

* [Flask](https://pypi.org/project/Flask/), Flask is a lightweight WSGI web application framework.
* [Flask-restful](https://flask-restful.readthedocs.io/en/latest/installation.html), Flask-restful is a package for developing restful api in flask.
* [Redis DB](https://redis.io/topics/quickstart), the key/value database
* [Redis py](https://pypi.org/project/redis/), the python package for redis
* [Redis Collections](https://pypi.org/project/redis-collections/), a package that implements collections stored in redis. Not used yet.

To install, just do:

``` bash
pip install redis-rest
```

Please, mind the "-" between words

## Description ##

A module for accessing a redis database by means of a restful api.

## Roadmap ##

Being under development, I am not sure about that this project
will be like in the future. These are my thought about the roadmap:

Redis, Flask and Flaskrest are great for Web programming, although:

* There are a few integration problems between them. For instance, 
a lot of type castings are required.

* AFAIK there is nothing like a DAO pattern to access redis though python
(not sure about if it is neccesary, just a thought)

* Redis objects (that is: string, list, hash, sets) are not the same as the ones
in python or available in json. Nor they have same names, for instance, the list
in redis is a linked list that behaves like a stack.

* There is no metadata information in redis, therefore, it is not possible to
know the type of the data or add some other information to it (permissions,
owner, last updated and so on). I mean it is possible only if you do it from
scratch.

In the future I will try to deal with these thoughts and convert them into
objectives, requirements and finally into solutions and source code.

## Getting Started ##

Up to now, just explore the code and review the examples at:

* [Redis Rest](https://bitbucket.org/stapia/redis-rest)

The example folder includes some html/javascripts files to manipulate the data base
using the API.
