#!/usr/bin/env python

import re
import sys
from os import path
from setuptools import setup, find_packages

requirements = [
    'redis',
    'redis-collections',
    'flask',
    'flask-restful'
]

version_file = path.join(
    path.dirname(__file__),
    'redis_rest',
    '__version__.py'
)
with open(version_file, 'r') as fp:
    m = re.search(
        r"^__version__ = ['\"]([^'\"]*)['\"]",
        fp.read(),
        re.M
    )
    version = m.groups(1)[0]

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='redis-rest',
    version=version,
    license='GPL3',
    url='https://bitbucket.org/stapia/redis-rest',
    author='Santiago Tapia-Fernández',
    author_email='santiago.tapia@upm.es',
    description='A module for accessing a redis database by means of a restful api',
    long_description=long_description,
    long_description_content_type="text/markdown",    
    packages=find_packages(exclude=['tests']),
    classifiers=[
        'Development Status :: 1 - Planning',
        'Topic :: Database :: Front-Ends',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Intended Audience :: Developers',
    ],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    test_suite = 'nose.collector',
    install_requires=requirements,
    python_requires='>=3.6',
    tests_require=['redis-rest'],
    # Install these with "pip install -e '.[docs]'
    extras_require={
       # 'docs': 'sphinx',
    }
)