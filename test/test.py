
import redis
import unittest


class TestingRedisInstallAndRunning(unittest.TestCase):

    def setUp(self):
        self.r = redis.Redis()

    def test_storeData(self):
        self.assertTrue(self.r.mset(
            {"Croatia": "Zagreb", "Bahamas": "Nassau", "Spain": "Madrid"}))

    def test_loadData(self):
        self.assertEqual(self.r.get("Bahamas"), b"Nassau")

if __name__ == '__main__':
    unittest.main()
