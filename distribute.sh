#!/bin/bash

# Build project
python3 setup.py sdist bdist_wheel

# Upload to test.pypi
python3 -m twine upload -u stapia -p $TEST_PYPI_PASSWORD --repository-url https://test.pypi.org/legacy/ dist/*
