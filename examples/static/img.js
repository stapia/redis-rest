
function guardarDato() {
    var clave = $("#clave").val();
    const the_file = document.getElementById('valor').files[0];
    console.log(`name: ${the_file.name}, size: ${the_file.size}`)
    var reader = new FileReader();

    reader.onload = function () {
        console.log("Imagen cargada de archivo");
        $.ajax({
            url: `/api/img/${clave}`,
            method: "POST",
            data: reader.result,
            processData: false,
            contentType: false,
            success: function (result) {
                console.log("Exito: %o", result);
                $("#result").html(`${JSON.stringify(result)}`);
            },
            error: function (result) {
                console.log("Error: %o", result);
                $("#result").html(`${JSON.stringify(result)}`);
            }
        });
    }
    reader.readAsArrayBuffer(the_file);
}

function recuperarDato() {
    var clave = $("#clave").val();
    console.log(`clave: '${clave}'`);
    const url = `/api/img/${clave}`;
    window.open(url);
}

function eliminarDato() {
    var clave = $("#clave").val();
    console.log(`clave: '${clave}'`);

    $.ajax({
        url: `/api/img/${clave}`,
        method: "DELETE",
        success: function (result) {
            console.log("Exito: %o", result);
        },
        error: function (result) {
            console.log("Error: %o", result);
        }
    });
}

function init() {
    console.log("Init");
    $("#guardarDato").click(guardarDato);
    $("#recuperarDato").click(recuperarDato);
    $("#eliminarDato").click(eliminarDato);
}

$(document).ready(init);

