

function guardarDato() {
    var clave = $("#clave").val();
    var valor = $("#valor").val();
    console.log(`clave: '${clave}', valor: '${valor}'`);

    $.ajax({
        url: `/api/list/${clave}`,
        method: "POST",
        data: valor,
        processData: false,  
        contentType: "application/json",   
        success: function (result) {
            console.log("Exito: %o", result);
            $("#result").html(`${JSON.stringify(result)}`);
        },
        error: function (result) {
            console.log("Error: %o", result);
            $("#result").html(`${JSON.stringify(result)}`);
        }
    });
}

function recuperarDato() {
    var clave = $("#clave").val();
    console.log(`clave: '${clave}'`);

    $.ajax({
        url: `/api/list/${clave}`,
        method: "GET",
        success: function (result) {
            console.log("Exito: %o", result);
            $("#valor").val(`${JSON.stringify(result)}`);
        },
        error: function (result) {
            console.log("Error: %o", result);
            $("#result").html(`${JSON.stringify(result)}`);
        }
    });
}

function eliminarDato() {
    var clave = $("#clave").val();
    console.log(`clave: '${clave}'`);

    $.ajax({
        url: `/api/list/${clave}`,
        method: "DELETE",
        success: function (result) {
            console.log("Exito: %o", result);
            $("#valor").val("BORRADO");
        },
        error: function (result) {
            console.log("Error: %o", result);
        }
    });
}

function init() {
    console.log("Init");
    $("#guardarDato").click(guardarDato);
    $("#recuperarDato").click(recuperarDato);
    $("#eliminarDato").click(eliminarDato);
}

$(document).ready(init);

