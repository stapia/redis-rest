
from flask import Flask, request
from flask_restful import Resource
# Not sure on using redis_collections
# from redis_collections import Dict


class DictResource(Resource):

    def __init__(self, **kwargs):
        self.app = kwargs['app']
        self.db = self.app.rdb
        self.app.logger.info("Initializing DictResource Handler")

    @staticmethod
    def to_str_dict(D: dict):
        result = dict()
        for key, value in D.items():
            result[key.decode("utf-8")] = value.decode("utf-8")
        return result

    def get(self, the_id):
        result = self.db.hgetall(the_id)
        self.app.logger.debug(f'Returning a dict: {result}')
        if result:
            return DictResource.to_str_dict(result)
        else:
            return "Does not exist"

    def post(self, the_id):
        value = request.get_json()
        if value and isinstance(value, dict):
            self.app.logger.debug(f'Storing a dict: name {the_id} / {value}')
            return self.db.hmset(the_id, value)
        else:
            return "Not a valid json object"

    def put(self, the_id):
        pass

    def delete(self, the_id):
        self.app.logger.debug(f'Deleting a dict: name {the_id}')
        return self.db.delete(the_id)
