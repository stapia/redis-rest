
from flask import Flask, request
from flask_restful import Resource


class ListResource(Resource):

    def __init__(self, **kwargs):
        self.app = kwargs['app']
        self.db = self.app.rdb
        self.app.logger.info("Initializing ListResource Handler")

    @staticmethod
    def to_str_list(L: list):
        return [ value.decode("utf-8") for value in L ]

    def get(self, the_id):
        result = self.db.lrange(the_id, 0, -1)
        self.app.logger.debug(f'Returning a list: {result}')
        if result:
            return ListResource.to_str_list(result)
        else:
            return "Does not exist"

    def post(self, the_id):
        value = request.get_json()
        if value and isinstance(value, list):
            self.app.logger.debug(f'Storing a list: name {the_id} / {value}')
            return self.db.lpush(the_id, *value)
        else:
            return "Not a valid json object"

    def put(self, the_id):
        pass

    def delete(self, the_id):
        self.app.logger.debug(f'Deleting a list: name {the_id}')
        return self.db.delete(the_id)
