
import redis
from flask import Flask
from flask_restful import Resource, Api

from .str_resource import StrResource
from .img_resource import ImgResource
from .dict_resource import DictResource
from .list_resource import ListResource


class RedisRest(Flask):
    def __init__(self, name, stand_alone):
        super().__init__(name)
        self.api = Api(self)
        self.rdb = redis.Redis()
        self.prefix = "/api" if stand_alone else ""

        self._add_resources()

        if stand_alone:
            super().run(debug=True)

    def _add_resources(self):
        self.api.add_resource(StrResource, self.prefix + '/str/<the_id>',
                              resource_class_kwargs={'app': self})

        self.api.add_resource(ImgResource, self.prefix + '/img/<the_id>',
                              resource_class_kwargs={'app': self})

        self.api.add_resource(DictResource, self.prefix + '/dict/<the_id>',
                              resource_class_kwargs={'app': self})

        self.api.add_resource(ListResource, self.prefix + '/list/<the_id>',
                              resource_class_kwargs={'app': self})
