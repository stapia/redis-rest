
from flask import Flask, request
from flask_restful import Resource

class ImgResource(Resource):

    def __init__(self, **kwargs):
        self.app = kwargs['app']
        self.db = self.app.rdb
        self.app.logger.info("Initializing ImgResource Handler")

    def get(self, the_id):
        result = self.db.get(the_id)
        if result:
            self.app.logger.debug('Returning an image')
            img_response = self.app.make_response(result)
            img_response.headers['Content-type'] = 'image/png'
            return img_response
        else:
            return "Does not exist"

    def post(self, the_id):
        value = request.data
        self.app.logger.debug(f'Storing an image: name {the_id} / value {type(value)}')
        return self.db.set(the_id, value)

    def put(self, the_id):
        value = request.data
        self.app.logger.debug(f'Storing an image: name {the_id} / value {type(value)}')
        return self.db.set(the_id, value)

    def delete(self, the_id):
        self.app.logger.debug(f'Deleting an image: name {the_id}')
        return self.db.delete(the_id)
