
from flask import request
from flask_restful import Resource

class StrResource(Resource):

    def __init__(self, **kwargs):
        self.app = kwargs['app']
        self.db = self.app.rdb
        self.app.logger.info("Initializing StrResource Handler")

    def get(self, the_id):
        result = self.db.get(the_id)
        if result:
            return result.decode('utf-8')
        else:
            return "No existe"

    def post(self, the_id):
        value = request.data
        self.app.logger.debug(f'Storing a str: name {the_id} / value {value}')
        return self.db.set(the_id, value)

    def put(self, the_id):
        value = request.data
        self.app.logger.debug(f'Storing a str: name {the_id} / value {value}')
        return self.db.set(the_id, value)

    def delete(self, the_id):
        self.app.logger.debug(f'Deleting a str: name {the_id}')
        return self.db.delete(the_id)
